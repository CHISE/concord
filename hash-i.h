/* Copyright (C) 2003, 2004, 2005, 2006, 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _HASH_I_H
#define _HASH_I_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

struct CONCORD_HASH_TABLE_ENTRY
{
  void *key;
  void *value;
};

struct CONCORD_HASH_TABLE
{
  size_t size;
  CONCORD_HASH_TABLE_ENTRY *data;
};

CONCORD_HASH_TABLE* concord_make_hash_table (size_t size);

void concord_destroy_hash_table (CONCORD_HASH_TABLE* hash);

unsigned long concord_hash_c_string (const unsigned char *ptr);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_HASH_I_H */
