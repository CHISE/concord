/* Copyright (C) 2003,2004,2005,2006,2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#include <string.h>
#include <stdlib.h>
#include "cos-i.h"

unsigned long concord_hash_c_string (const unsigned char *ptr);
unsigned long cos_hash_c_string_n (const unsigned char *ptr, size_t size);
unsigned long cos_symbol_hash_string (COS_String string);


COS_String_ent cos_string_ent_nil = { {COS_OBJECT_PREFIX_OBJECT,
				       COS_Object_Type_String,
				       1},
				      3, "nil"};

COS_Symbol_ent cos_symbol_ent_nil = { {COS_OBJECT_PREFIX_OBJECT,
				       COS_Object_Type_Symbol,
				       1},
				      &cos_string_ent_nil,
				      NULL};

COS_Symbol cos_Qnil = &cos_symbol_ent_nil;


COS_String_ent cos_string_ent_t = { {COS_OBJECT_PREFIX_OBJECT,
				     COS_Object_Type_String,
				     1},
				    1, "t"};

COS_Symbol_ent cos_symbol_ent_t = { {COS_OBJECT_PREFIX_OBJECT,
				     COS_Object_Type_Symbol,
				     1},
				    &cos_string_ent_t,
				    NULL};

COS_Symbol cos_Qt = &cos_symbol_ent_t;


COS_String_ent cos_string_ent_composition = { {COS_OBJECT_PREFIX_OBJECT,
					       COS_Object_Type_String,
					       1},
					      3, "composition"};

COS_Symbol_ent cos_symbol_ent_composition = { {COS_OBJECT_PREFIX_OBJECT,
					       COS_Object_Type_Symbol,
					       1},
					      &cos_string_ent_composition,
					      NULL};

COS_Symbol cos_Qcomposition = &cos_symbol_ent_composition;


COS_Symbol_Table cos_default_symbol_table = NULL;


COS_Symbol
cos_make_symbol (COS_String string)
{
  COS_Symbol obj = COS_ALLOCATE_OBJECT (Symbol);

  if (obj == NULL)
    return NULL;

  obj->name = string;
  obj->value = NULL;
  cos_retain_object (string);
  return obj;
}

int
cos_retain_symbol (COS_Object obj)
{
  //cos_retain_object (((COS_Symbol)obj)->value);
  //cos_retain_object (((COS_Symbol)obj)->name);
  return 0;
}

int
cos_release_symbol (COS_Object obj)
{
  if (obj == NULL)
    return 0;

  if ( (obj == cos_Qnil) || (obj == cos_Qt) ||
       (obj == cos_Qcomposition) )
    return 0;

  if ( ((COS_Symbol)obj)->value != NULL)
    cos_release_object (((COS_Symbol)obj)->value);

  cos_release_object (((COS_Symbol)obj)->name);
  free (obj);
  return 0;
}

int
cos_symbol_p (COS_object obj)
{
  return COS_OBJECT_SYMBOL_P (obj);
}

COS_Symbol
cos_symbol_table_intern (COS_Symbol_Table table, COS_object name)
{
  unsigned char* key;
  COS_String key_string;
  unsigned long i, index;
  COS_Symbol entry;

  if (table == NULL)
    return NULL;

  if (COS_OBJECT_C_STRING_P (name))
    {
      key_string = cos_build_string ((char*)name);
    }
  else
    {
      key_string = (COS_String)name;
    }
  key = key_string->data;
  index = cos_symbol_hash_string (key_string) % table->size;
  for (i = index; i < table->size; i++)
    {
      entry = table->data[i];
      if (entry == NULL)
	{
	  entry = cos_make_symbol (key_string);
	  cos_retain_object ((COS_object)entry);
	  table->data[i] = entry;
	  return entry;
	}
      else if ( COS_OBJECT_SYMBOL_P (entry)
		&& COS_OBJECT_STRING_P (entry->name)
		&& (entry->name->size == key_string->size)
		&& (memcmp (entry->name->data,
			    key_string->data, key_string->size) == 0) )
	{
	  cos_retain_object ((COS_object)entry);
	  return entry;
	}
    }
  if (cos_symbol_table_grow (table) == 0)
    return cos_symbol_table_intern (table, key_string);
  return NULL;
}

COS_String
cos_symbol_name (COS_Symbol symbol)
{
  return symbol->name;
}


COS_Symbol_Table cos_make_symbol_table_0 (size_t size);
void cos_destroy_symbol_table_0 (COS_Symbol_Table hash);


/* derived from hashpjw, Dragon Book P436. */
unsigned long
cos_hash_c_string_n (const unsigned char *ptr, size_t size)
{
  unsigned long hash = 0;
  int i;

  for ( i = 0; i < size; i++ )
    {
      unsigned long g;
      hash = (hash << 4) + ptr[i];
      g = hash & 0xf0000000;
      if (g)
	hash = (hash ^ (g >> 24)) ^ g;
    }
  return hash & 07777777777;
}

unsigned long
cos_symbol_hash_string (COS_String string)
{
  return cos_hash_c_string_n (string->data, string->size);
}


COS_Symbol_Table
cos_make_symbol_table_0 (size_t size)
{
  COS_Symbol_Table hash
    = (COS_Symbol_Table)malloc (sizeof (COS_Symbol_Table_ent));

  if (hash == NULL)
    return NULL;

  hash->data
    = (COS_Symbol*) malloc (sizeof (COS_Symbol) * size);
  if (hash->data == NULL)
    {
      free (hash);
      return NULL;
    }

  hash->size = size;
  memset (hash->data, 0, sizeof (COS_Symbol) * size);
  return hash;
}

void
cos_destroy_symbol_table_0 (COS_Symbol_Table table)
{
  if (table == NULL)
    return;
  free (table->data);
  free (table);
}


COS_Symbol_Table
cos_make_symbol_table ()
{
  return cos_make_symbol_table_0 (2 /* 256 */);
}

void
cos_destroy_symbol_table (COS_Symbol_Table table)
{
  int i;

  for (i = 0; i < table->size; i++)
    {
      COS_Symbol entry = table->data[i];

      if (entry != NULL)
	{
	  if (entry->name != NULL)
	    cos_release_object (entry->name);
	  if (entry->value != NULL)
	    cos_release_object (entry->value);
	  free (entry);
	}
    }
  cos_destroy_symbol_table_0 (table);
}

COS_Symbol
cos_intern (COS_object name)
{
  if (cos_default_symbol_table == NULL)
    {
      cos_default_symbol_table = cos_make_symbol_table();
      cos_symbol_table_set (cos_default_symbol_table, cos_Qnil);
      cos_symbol_table_set (cos_default_symbol_table, cos_Qt);
      cos_symbol_table_set (cos_default_symbol_table, cos_Qcomposition);
    }
  return cos_symbol_table_intern (cos_default_symbol_table, name);
}

int
cos_symbol_table_set (COS_Symbol_Table table, COS_Symbol symbol)
{
  int i, index;
  COS_Symbol entry;

  if (table == NULL)
    return -1;

  index = cos_symbol_hash_string (symbol->name) % table->size;
  for (i = index; i < table->size; i++)
    {
      entry = table->data[i];
      if (entry == NULL)
	{
	  entry = symbol;
	  cos_retain_object ((COS_object)entry);
	  table->data[i] = entry;
	  return 0;
	}
      else if (entry == symbol)
	{
	  return 0;
	}
    }
  if (cos_symbol_table_grow (table) == 0)
    return cos_symbol_table_set (table, symbol);
  return -1;
}

int
cos_print_symbol_table (COS_Symbol_Table table)
{
  int i;
  COS_Symbol entry;

  if (table == NULL)
    table = cos_default_symbol_table;

  printf ("#[symbol_table %lX\tsize = %d", table->size);
  for (i = 0; i < table->size; i++)
    {
      entry = table->data[i];
      printf ("\n\t%d : ", i);
      cos_print_object (entry);
    }
  printf ("\t]\n");
  return 0;
}

int
cos_symbol_table_grow (COS_Symbol_Table table)
{
  COS_Symbol_Table new_table
    = cos_make_symbol_table_0 ( table->size * 2
				/* - (table->size * 4 / 5) */ );
  int i;

  if (new_table == NULL)
    return -1;

  //printf ("\n(old table is ");
  //cos_print_symbol_table (table);
  for (i = 0; i < table->size; i++)
    {
      COS_Symbol entry = table->data[i];

      if (entry != NULL)
	{
	  int status = cos_symbol_table_set (new_table, entry);

	  if (status != 0)
	    {
	      cos_destroy_symbol_table_0 (new_table);
	      return -1;
	    }
	}
    }
  free (table->data);
  table->size = new_table->size;
  table->data = new_table->data;
  free (new_table);
  //printf ("\t)\n(new table is ");
  //cos_print_symbol_table (table);
  //printf ("\t)\n");
  return 0;
}
