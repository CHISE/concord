/* Types and functions of CONCORD Object System

   Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _COS_H
#define _COS_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

#include <stdlib.h>
#include <stdint.h>


typedef void* COS_object;

typedef struct COS_Object_ent COS_Object_ent;
typedef COS_Object_ent* COS_Object;

COS_Object cos_retain_object (COS_Object obj);
int cos_release_object (COS_object obj);


typedef intptr_t COS_C_Int;

COS_object cos_make_int (COS_C_Int num);
int cos_int_p (COS_object obj);
COS_C_Int cos_int_value (COS_object obj);


COS_object cos_make_char (int code);
int cos_char_p (COS_object obj);
int cos_char_id (COS_object obj);


typedef struct COS_String_ent COS_String_ent;
typedef struct COS_String_ent* COS_String;

COS_String cos_make_string (char* str, size_t size);
COS_String cos_build_string (char* str);

int cos_string_p (COS_object obj);

size_t cos_string_size (COS_String string);
char* cos_string_data (COS_String string);


typedef struct COS_Symbol_ent COS_Symbol_ent;
typedef struct COS_Symbol_ent* COS_Symbol;

COS_Symbol cos_intern (COS_object name);

int cos_symbol_p (COS_object obj);

COS_String cos_symbol_name (COS_Symbol symbol);

extern COS_Symbol cos_Qnil;
extern COS_Symbol cos_Qt;
extern COS_Symbol cos_Qcomposition;

#define COS_NIL	cos_Qnil
#define COS_T	cos_Qt
#define COS_COMPOSITION cos_Qcomposition


typedef struct COS_Cons_ent COS_Cons_ent;
typedef struct COS_Cons_ent* COS_Cons;

COS_Cons cos_cons (COS_object car, COS_object cdr);
int cos_cons_p (COS_object obj);
COS_object cos_car (COS_Cons pair);
COS_object cos_cdr (COS_Cons pair);

COS_Cons cos_assoc (COS_object key, COS_Cons alist);
COS_object cos_alist_get (COS_Cons alist, COS_object key);


typedef struct COS_Container_ent COS_Container_ent;
typedef struct COS_Container_ent* COS_Container;


typedef struct COS_Sexp_ent COS_Sexp_ent;
typedef struct COS_Sexp_ent* COS_Sexp;


typedef struct COS_Binary_ent COS_Binary_ent;
typedef struct COS_Binary_ent* COS_Binary;


void cos_print_object (COS_object obj);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_COS_H */
