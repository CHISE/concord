#include <stdio.h>
#include "cos.h"
#include "cos-hash.h"

int
main (int argc, char* argv[])
{
  COS_Hash_Table ht;
  COS_object obj;

  ht = cos_make_hash_table ();
  if (ht == NULL)
    {
      printf ("Can't make hash table.\n");
      return -1;
    }

  cos_hash_table_put (ht, cos_make_char ('A'),
		      cos_build_string ("character A"));
  cos_hash_table_put (ht, cos_make_char ('a'),
		      cos_build_string ("character a"));
  cos_hash_table_put (ht, cos_make_int (65),
		      cos_build_string ("int 65"));
  cos_hash_table_put (ht, cos_intern ("sym"),
		      cos_build_string ("symbol `sym'"));

  obj = cos_make_char ('A');
  cos_print_object (obj);
  printf (" => ");
  cos_print_object (cos_hash_table_get (ht, obj));
  printf ("\n");

  obj = cos_make_char ('a');
  cos_print_object (obj);
  printf (" => ");
  cos_print_object (cos_hash_table_get (ht, obj));
  printf ("\n");

  obj = cos_make_int (65);
  cos_print_object (obj);
  printf (" => ");
  cos_print_object (cos_hash_table_get (ht, obj));
  printf ("\n");

  obj = cos_intern ("sym");
  cos_print_object (obj);
  printf (" => ");
  cos_print_object (cos_hash_table_get (ht, obj));
  printf ("\n");

  obj = cos_intern ("symbol");
  cos_print_object (obj);
  printf (" => ");
  cos_print_object (cos_hash_table_get (ht, obj));
  printf ("\n");

  return 0;
}
