/* Internal definitions of CONCORD Object System

   Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _COS_I_H
#define _COS_I_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

#include "sysdep.h"
#include "concord.h"
#include "cos.h"
#include "cos-hash.h"

#ifndef SIZEOF_COS_INT
# define SIZEOF_COS_INT SIZEOF_VOID_P
#endif

#ifndef SIZEOF_INT
# define SIZEOF_INT (sizeof (int))
#endif

#ifndef SIZEOF_LONG
# define SIZEOF_LONG (sizeof (long))
#endif

#ifndef SIZEOF_LONG_LONG
# define SIZEOF_LONG_LONG (sizeof (long long))
#endif

#ifndef COS_INT
# if SIZEOF_COS_INT == SIZEOF_LONG
#  define COS_INT long
# elif SIZEOF_COS_INT == SIZEOF_INT
#  define COS_INT int
# elif SIZEOF_COS_INT == SIZEOF_LONG_LONG
#  define COS_INT long long
# else
#  error Unable to determine suitable type for COS_INT
# endif
#endif

#ifndef COS_UINT
# define COS_UINT unsigned COS_INT
#endif

#define BITS_PER_COS_INT (SIZEOF_COS_INT * BITS_PER_CHAR)


enum COS_Type {
  COS_Type_Entity,
  COS_Type_Int_Even,
  COS_Type_Char,
  COS_Type_Int_Odd
};

#define COS_POINTER_TYPE_P(type) ((type) == COS_Type_Entity)

#define COS_TYPE_BITS  2
#define COS_INT_TYPE_BITS  1

#define COS_INT_VAL_BITS (BITS_PER_COS_INT - COS_INT_TYPE_BITS)
#define COS_VAL_BITS (BITS_PER_COS_INT - COS_TYPE_BITS)
#define COS_INT_MAX ((COS_INT) ((1UL << (COS_INT_VAL_BITS - 1)) -1UL))
#define COS_INT_MIN (-(COS_INT_MAX) - 1)


typedef struct COS_Object_Header
{
  unsigned char prefix;
  unsigned char type;
  COS_INT	reference_count;
} COS_Object_Header;

#define COS_OBJECT_PREFIX_OBJECT	  0

enum COS_Object_Type {
  COS_Object_Type_NULL,
  COS_Object_Type_C_String,
  COS_Object_Type_int,
  COS_Object_Type_char,
  COS_Object_Type_String,
  COS_Object_Type_Symbol,
  COS_Object_Type_Cons,
  COS_Object_Type_Container,
  COS_Object_Type_Sexp,
  COS_Object_Type_Binary,
  COS_Object_Type_DS,
  COS_Object_Type_Genre,
  COS_Object_Type_Feature,
  COS_Object_Type_Feature_INDEX,
  COS_Object_Type_DB_Object
};

#define COS_FAT_OBJECT_TYPE_MIN COS_Object_Type_String
#define COS_OBJECT_TYPE_MAX COS_Object_Type_DB_Object

extern int (*COS_Object_retain_function_table[]) (COS_Object);
extern int (*COS_Object_release_function_table[]) (COS_Object);

struct COS_Object_ent
{
  COS_Object_Header header;
};

COS_Object cos_allocate_object_0 (enum COS_Object_Type type,
				  size_t size);

#define COS_ALLOCATE_OBJECT(type) \
  ((COS_##type)cos_allocate_object_0(COS_Object_Type_##type, \
				     sizeof(COS_##type##_ent)))

#define COS_OBJECT_INT_P(obj) \
  ((COS_UINT)(obj) & 1)

#define COS_OBJECT_CHAR_P(obj) \
  ((COS_UINT)(obj) & 2)

#define COS_OBJECT_C_STRING_P(obj) \
  ((obj != NULL) \
   && (((unsigned char *)obj)[0] != COS_OBJECT_PREFIX_OBJECT))

#define COS_OBJECT_P(obj) \
  ((obj != NULL) \
   && (!COS_OBJECT_INT_P (obj)) \
   && (!COS_OBJECT_CHAR_P (obj)) \
   && (((unsigned char *)obj)[0] == COS_OBJECT_PREFIX_OBJECT))

#define COS_OBJECT_TYPE_P(obj, TYPE) \
  (COS_OBJECT_P (obj) \
   && (((COS_Object)obj)->header.type == COS_Object_Type_##TYPE))

#define COS_OBJECT_STRING_P(obj) \
  COS_OBJECT_TYPE_P (obj, String)

#define COS_OBJECT_SYMBOL_P(obj) \
  COS_OBJECT_TYPE_P (obj, Symbol)

#define COS_OBJECT_CONS_P(obj) \
  COS_OBJECT_TYPE_P (obj, Cons)

#define COS_OBJECT_DS_P(obj) \
  COS_OBJECT_TYPE_P (obj, DS)

#define COS_OBJECT_GENRE_P(obj) \
  COS_OBJECT_TYPE_P (obj, Genre)

#define COS_OBJECT_FEATURE_P(obj) \
  COS_OBJECT_TYPE_P (obj, Feature)

#define COS_OBJECT_FEATURE_INDEX_P(obj) \
  COS_OBJECT_TYPE_P (obj, Feature_INDEX)


struct COS_String_ent
{
  COS_Object_Header header;

  size_t size;
  unsigned char* data;
};

int cos_retain_string (COS_Object obj);
int cos_release_string (COS_Object obj);


struct COS_Symbol_ent
{
  COS_Object_Header header;

  COS_String name;
  COS_object value;
};

COS_Symbol cos_make_symbol (COS_String string);

int cos_retain_symbol (COS_Object obj);
int cos_release_symbol (COS_Object obj);


typedef struct COS_Symbol_Table_ent COS_Symbol_Table_ent;
typedef COS_Symbol_Table_ent* COS_Symbol_Table;

struct COS_Symbol_Table_ent
{
  size_t size;
  COS_Symbol* data;
};

COS_Symbol_Table cos_make_symbol_table (void);

int cos_symbol_table_grow (COS_Symbol_Table table);

void cos_destroy_symbol_table (COS_Symbol_Table table);

int cos_symbol_table_set (COS_Symbol_Table table, COS_Symbol symbol);

COS_Symbol
cos_symbol_table_intern (COS_Symbol_Table table, COS_object name);


struct COS_Cons_ent
{
  COS_Object_Header header;

  COS_object car;
  COS_object cdr;
};

#define COS_CAR(obj) \
  (((COS_Cons)(obj))->car)
#define COS_CDR(obj) \
  (((COS_Cons)(obj))->cdr)

int cos_retain_cons (COS_Object obj);
int cos_release_cons (COS_Object obj);


struct COS_Container_ent
{
  COS_Object_Header header;

  COS_Object type;
  size_t size;
  size_t len;
  size_t offset;
  COS_Object* data;
};

int cos_retain_container (COS_Object obj);
int cos_release_container (COS_Object obj);


struct COS_Sexp_ent
{
  COS_Object_Header header;

  size_t len;
  char* data;
};

int cos_retain_sexp (COS_Object obj);
int cos_release_sexp (COS_Object obj);


struct COS_Binary_ent
{
  COS_Object_Header header;

  size_t len;
  unsigned char* data;
};

int cos_retain_binary (COS_Object obj);
int cos_release_binary (COS_Object obj);


int cos_retain_ds (COS_Object obj);
int cos_release_ds (COS_Object obj);


int cos_retain_genre (COS_Object obj);
int cos_release_genre (COS_Object obj);

int concord_close_genre (COS_Genre genre);


struct COS_Feature_ent
{
  COS_Object_Header header;
  CONCORD_Genre genre;
  char* name;
  DB* db;
  u_int32_t access;
  COS_Hash_Table value_table;
  COS_Feature next;
  unsigned short number_of_hits;
#if 0
  unsigned short number_of_closes;
#endif
};

int cos_retain_feature (COS_Object obj);
int cos_release_feature (COS_Object obj);

int concord_close_feature (COS_Feature feature);


struct COS_Feature_INDEX_ent
{
  COS_Object_Header header;
  CONCORD_Genre genre;
  char *name;
  DB* db;
  u_int32_t access;
  COS_Hash_Table decoding_table;
  COS_Feature_INDEX next;
};

int cos_retain_index (COS_Object obj);
int cos_release_index (COS_Object obj);

int concord_close_index (COS_Feature_INDEX table);


struct COS_DB_Object_ent
{
  COS_Object_Header header;

  COS_Feature feature;
  COS_Object id;
};

int cos_retain_db_object (COS_Object obj);
int cos_release_db_object (COS_Object obj);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_COS_I_H */
