/* Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#include "cos-print.h"
#include "cos-i.h"

int
cos_utf8_encode_char (int cid, unsigned char *dest, size_t size)
{
  int i = 0;

  if (cid <= 0x7F)
    {
      if (size >= 2)
	{
	  dest[i++] = cid;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
  else if (cid <= 0x7FF)
    {
      if (size >= 3)
	{
	  dest[i++] = (cid >> 6) | 0xC0;
	  dest[i++] = (cid & 0x3F) | 0x80;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
  else if (cid <= 0xFFFF)
    {
      if (size >= 4)
	{
	  dest[i++] = (cid >> 12) | 0xE0;
	  dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
	  dest[i++]=  (cid        & 0x3F) | 0x80;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
  else if (cid <= 0x1FFFFF)
    {
      if (size >= 5)
	{
	  dest[i++]=  (cid >> 18) | 0xF0;
	  dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
	  dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
	  dest[i++]=  (cid        & 0x3F) | 0x80;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
  else if (cid <= 0x3FFFFFF)
    {
      if (size >= 6)
	{
	  dest[i++]=  (cid >> 24) | 0xF8;
	  dest[i++]= ((cid >> 18) & 0x3F) | 0x80;
	  dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
	  dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
	  dest[i++]=  (cid        & 0x3F) | 0x80;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
  else
    {
      if (size >= 7)
	{
	  dest[i++]=  (cid >> 30) | 0xFC;
	  dest[i++]= ((cid >> 24) & 0x3F) | 0x80;
	  dest[i++]= ((cid >> 18) & 0x3F) | 0x80;
	  dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
	  dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
	  dest[i++]=  (cid        & 0x3F) | 0x80;
	  dest[i] = '\0';
	  return i;
	}
      else
	return -1;
    }
}

int
cos_utf8_print_char (COS_object character, unsigned char *dest, size_t size)
{
  int cid = cos_char_id (character);
  int i = 0;

  dest[i++] = '?';
  if (cid == '\t')
    {
      dest[i++] = '\\';
      dest[i++] = 't';
      dest[i] = '\0';
      return i;
    }
  else if (cid == '\n')
    {
      dest[i++] = '\\';
      dest[i++] = 'n';
      dest[i] = '\0';
      return i;
    }
  else if (cid == '\r')
    {
      dest[i++] = '\\';
      dest[i++] = 'r';
      dest[i] = '\0';
      return i;
    }
  else if (cid == 0x1C)
    {
      dest[i++] = '\\';
      dest[i++] = '^';
      dest[i++] = '\\';
      dest[i++] = '\\';
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x1F)
    {
      dest[i++] = '\\';
      dest[i++] = '^';
      dest[i++] = '@' + cid;
      dest[i] = '\0';
      return i;
    }
  else if ( (cid == ' ') || (cid == '"') ||
	    (cid == '#') || (cid == '\'') ||
	    (cid == '(') || (cid == ')') ||
	    (cid == ',') || (cid == '.') ||
	    (cid == ';') || (cid == '?') ||
	    (cid == '[') || (cid == '\\') ||
	    (cid == ']') || (cid == '`') )
    {
      dest[i++] = '\\';
      dest[i++] = cid;
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x7E)
    {
      dest[i++] = cid;
      dest[i] = '\0';
      return i;
    }
  else if (cid == 0x7F)
    {
      dest[i++] = '\\';
      dest[i++] = '^';
      dest[i++] = '?';
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x9F)
    {
      dest[i++] = '\\';
      dest[i++] = '^';
      dest[i++] = ((cid + '@') >> 6) | 0xC0;
      dest[i++] = ((cid + '@') & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x7FF)
    {
      dest[i++] = (cid >> 6) | 0xC0;
      dest[i++] = (cid & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0xFFFF)
    {
      dest[i++] = (cid >> 12) | 0xE0;
      dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
      dest[i++]=  (cid        & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x1FFFFF)
    {
      dest[i++]=  (cid >> 18) | 0xF0;
      dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
      dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
      dest[i++]=  (cid        & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
  else if (cid <= 0x3FFFFFF)
    {
      dest[i++]=  (cid >> 24) | 0xF8;
      dest[i++]= ((cid >> 18) & 0x3F) | 0x80;
      dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
      dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
      dest[i++]=  (cid        & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
  else
    {
      dest[i++]=  (cid >> 30) | 0xFC;
      dest[i++]= ((cid >> 24) & 0x3F) | 0x80;
      dest[i++]= ((cid >> 18) & 0x3F) | 0x80;
      dest[i++]= ((cid >> 12) & 0x3F) | 0x80;
      dest[i++]= ((cid >>  6) & 0x3F) | 0x80;
      dest[i++]=  (cid        & 0x3F) | 0x80;
      dest[i] = '\0';
      return i;
    }
}


void
cos_print_object (COS_object obj)
{
  if ( obj == NULL )
    printf ("#<NULL>", obj);
  else if ( COS_OBJECT_INT_P (obj) )
    printf ("%d", cos_int_value (obj));
  else if ( COS_OBJECT_CHAR_P (obj) )
    {
      char id_buf[256];

      cos_utf8_print_char (obj, id_buf, 256);
      printf ("%s", id_buf);
    }
  else if ( COS_OBJECT_STRING_P (obj) )
    {
      printf ("\"%s\"", cos_string_data (obj));
    }
  else if ( COS_OBJECT_SYMBOL_P (obj) )
    {
      printf ("%s", cos_string_data (cos_symbol_name (obj)));
    }
  else if ( COS_OBJECT_CONS_P (obj) )
    {
      COS_object rest = COS_CDR (obj);

      printf ("(");
      cos_print_object (COS_CAR (obj));
      while ( COS_OBJECT_CONS_P (rest) )
	{
	  printf (" ");
	  cos_print_object (COS_CAR (rest));
	  rest = COS_CDR (rest);
	}
      if ( rest != cos_Qnil )
	{
	  printf (" . ");
	  cos_print_object (rest);
	}
      printf (")");
    }
  else
    printf ("Object[0x%lX] is a fat object.\n",
	    obj);
}
