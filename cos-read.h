/* Reader functions of CONCORD Object System

   Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _COS_READ_H
#define _COS_READ_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

#include "cos.h"

COS_object
cos_read_int (unsigned char *str, size_t len, size_t start, size_t* endp);

int
cos_read_utf8 (unsigned char *str, size_t len, size_t start, size_t* endp);

int
cos_read_char (unsigned char *str, size_t len, size_t start, size_t* endp);

COS_String
cos_read_string (unsigned char *str, size_t len, size_t start, size_t* endp);

COS_Symbol
cos_read_symbol (unsigned char *str, size_t len, size_t start, size_t* endp);

COS_Cons
cos_read_list (unsigned char *str, size_t len, size_t start, size_t* endp);

COS_object
cos_read_object (unsigned char *str, size_t len, size_t start, size_t* endp);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_COS_READ_H */
