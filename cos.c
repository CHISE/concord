/* Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#include <stdlib.h>
#include "sysdep.h"
#include "cos-i.h"
#include "cos-read.h"
#include "cos-print.h"

const char concord_db_format_version[] = CONCORD_DB_FORMAT_VERSION;
const char concord_db_dir[] = CONCORD_DB_DIR;
const char concord_system_db_dir[] = CONCORD_SI_DB_DIR;

CONCORD_DS concord_current_env = NULL;

int (*COS_Object_retain_function_table
     [COS_OBJECT_TYPE_MAX - COS_Object_Type_char])
  (COS_Object)
  = { cos_retain_string,
      cos_retain_symbol,
      cos_retain_cons,
      cos_retain_container,
      cos_retain_sexp,
      cos_retain_binary,
      cos_retain_ds,
      cos_retain_genre,
      cos_retain_feature,
      cos_retain_index,
      cos_retain_db_object
};

int (*COS_Object_release_function_table
     [COS_OBJECT_TYPE_MAX - COS_Object_Type_char])
  (COS_Object)
  = { cos_release_string,
      cos_release_symbol,
      cos_release_cons,
      cos_release_container,
      cos_release_sexp,
      cos_release_binary,
      cos_release_ds,
      cos_release_genre,
      cos_release_feature,
      cos_release_index,
      cos_release_db_object
};


COS_object
cos_make_int (COS_C_Int num)
{
  return (COS_object)((COS_INT)(num << 1) | 1);
}

int
cos_int_p (COS_object obj)
{
  return COS_OBJECT_INT_P (obj);
}

COS_C_Int
cos_int_value (COS_object obj)
{
  if (COS_OBJECT_INT_P (obj))
    return ((COS_INT)obj) >> 1;
  return INTPTR_MIN;
}


COS_object
cos_make_char (int code)
{
  return (COS_object)((COS_INT)(code << 2) | 2);
}

int
cos_char_p (COS_object obj)
{
  return COS_OBJECT_CHAR_P (obj);
}

int
cos_char_id (COS_object obj)
{
  if (COS_OBJECT_CHAR_P (obj))
    return (((COS_INT)obj) >> 2) & 0x7FFFFFFF;
  return -1;
}


COS_Object
cos_allocate_object_0 (enum COS_Object_Type type, size_t size)
{
  COS_Object obj = (COS_Object)malloc (size);

  if (obj == NULL)
    return NULL;

  obj->header.prefix = COS_OBJECT_PREFIX_OBJECT;
  obj->header.type = type;
  obj->header.reference_count = 0;

  return obj;
}

COS_Object
cos_retain_object (COS_Object obj)
{
  if (COS_OBJECT_P (obj))
    {
      obj->header.reference_count++;
      (*COS_Object_retain_function_table
       [((COS_Object)obj)->header.type
	- COS_FAT_OBJECT_TYPE_MIN])(obj);
    }
  return obj;
}

int
cos_release_object (COS_object obj)
{
  if (COS_OBJECT_P (obj))
    {
      ((COS_Object)obj)->header.reference_count--;
      if ( ((COS_Object)obj)->header.reference_count <= 0 )
	return (*COS_Object_release_function_table
		[((COS_Object)obj)->header.type
		 - COS_FAT_OBJECT_TYPE_MIN])(obj);
    }
  return 0;
}


COS_String
cos_make_string (char* str, size_t size)
{
  COS_String obj = COS_ALLOCATE_OBJECT (String);

  if (obj == NULL)
    return NULL;

  obj->size = size;
  obj->data = malloc (size + 1);
  if (obj->data == NULL)
    {
      free (obj);
      return NULL;
    }

  strncpy ((char*)obj->data, str, size);
  obj->data[size] = '\0';
  return obj;
}

COS_String
cos_build_string (char* str)
{
#if 0
  COS_String obj = COS_ALLOCATE_OBJECT (string);

  if (obj == NULL)
    return NULL;

  obj->size = strlen (str);
  obj->data = malloc (obj->size + 1);
  if (obj->data == NULL)
    {
      free (obj);
      return NULL;
    }

  strncpy ((char*)obj->data, str, obj->size);
  obj->data[obj->size] = '\0';
  return obj;
#else
  return cos_make_string (str, strlen (str));
#endif
}

int
cos_retain_string (COS_Object obj)
{
  return 0;
}

int
cos_release_string (COS_Object obj)
{
  if (obj == NULL)
    return 0;

  if ( ((COS_String)obj)->data != NULL)
    free (((COS_String)obj)->data);
  free (obj);
  return 0;
}

int cos_string_p (COS_object obj)
{
  return COS_OBJECT_STRING_P (obj);
}

size_t
cos_string_size (COS_String string)
{
  return string->size;
}

char*
cos_string_data (COS_String string)
{
  return (char*)string->data;
}


COS_Cons
cos_cons (COS_object car, COS_object cdr)
{
  COS_Cons obj = COS_ALLOCATE_OBJECT (Cons);

  if (obj == NULL)
    return NULL;

  obj->car = car;
  obj->cdr = cdr;
  cos_retain_object (car);
  cos_retain_object (cdr);

  return obj;
}

int
cos_retain_cons (COS_Object obj)
{
  //cos_retain_object (COS_CAR (obj));
  //cos_retain_object (COS_CDR (obj));
  return 0;
}

int
cos_release_cons (COS_Object obj)
{
  if (obj == NULL)
    return 0;

  cos_release_object (COS_CAR (obj));
  cos_release_object (COS_CDR (obj));
  free (obj);
  return 0;
}

int
cos_cons_p (COS_object obj)
{
  return COS_OBJECT_CONS_P (obj);
}

COS_object
cos_car (COS_Cons pair)
{
  if (COS_OBJECT_CONS_P (pair))
    return COS_CAR (pair);
  else
    return NULL;
}

COS_object
cos_cdr (COS_Cons pair)
{
  if (COS_OBJECT_CONS_P (pair))
    return COS_CDR (pair);
  else
    return NULL;
}

COS_Cons
cos_assoc (COS_object key, COS_Cons alist)
{
  COS_Cons rest = alist;

  while ( COS_OBJECT_CONS_P (rest) )
    {
      COS_Cons cell = cos_car (rest);

      if ( cos_car (cell) == key )
	return cell;

      rest = cos_cdr (rest);
    }
  return NULL;
}

COS_object
cos_alist_get (COS_Cons alist, COS_object key)
{
  return cos_cdr (cos_assoc (key, alist));
}


int
cos_retain_container (COS_Object obj)
{
  return 0;
}

int
cos_release_container (COS_Object obj)
{
  return 0;
}


int
cos_retain_sexp (COS_Object obj)
{
  return 0;
}

int
cos_release_sexp (COS_Object obj)
{
  return 0;
}


int
cos_retain_binary (COS_Object obj)
{
  return 0;
}

int
cos_release_binary (COS_Object obj)
{
  return 0;
}


COS_DS
concord_open_env (COS_object ds)
{
  if (COS_OBJECT_DS_P (ds))
    concord_current_env = (COS_DS)ds;
  else
    {
      char* path;

      if (COS_OBJECT_C_STRING_P (ds))
	path = (char*)ds;
      else if (ds == NULL)
	path = CONCORD_SI_DB_DIR;
      else
	return NULL;

      concord_current_env = concord_open_ds (CONCORD_Backend_Berkeley_DB,
					     path, 0, 0755);
    }
  return concord_current_env;
}

int
cos_retain_ds (COS_Object obj)
{
  return 0;
}

int
cos_release_ds (COS_Object obj)
{
  return concord_close_ds ((COS_DS)obj);
}

int
concord_ds_p (COS_object obj)
{
  return COS_OBJECT_DS_P (obj);
}


COS_Genre
concord_get_genre (COS_object ds, COS_object genre)
{
  if (COS_OBJECT_C_STRING_P (genre))
    return concord_ds_get_genre (ds, (char*)genre);
  else if (COS_OBJECT_SYMBOL_P (genre))
    return concord_ds_get_genre (ds, (char*)((COS_Symbol)genre)->name->data);
  else if (COS_OBJECT_GENRE_P (genre))
    return (COS_Genre)genre;
  return NULL;
}

int
cos_retain_genre (COS_Object obj)
{
  return 0;
}

int
cos_release_genre (COS_Object obj)
{
  return concord_close_genre ((COS_Genre)obj);
}


COS_Feature
concord_get_feature (COS_object ds,
		     COS_object genre, COS_object feature)
{
  if (COS_OBJECT_FEATURE_P (feature))
    return feature;
  else
    {
      COS_Genre gobj = concord_get_genre (ds, genre);
      char* feature_name;

      if (COS_OBJECT_C_STRING_P (feature))
	feature_name = (char*)feature;
      else if (COS_OBJECT_STRING_P (feature))
	feature_name = (char*)((COS_String)feature)->data;
      else if (COS_OBJECT_SYMBOL_P (feature))
	feature_name = (char*)((COS_Symbol)feature)->name->data;
      else
	return NULL;

      return concord_genre_get_feature (gobj, feature_name);
    }
}

int
cos_retain_feature (COS_Object obj)
{
  return 0;
}

int
cos_release_feature (COS_Object obj)
{
  return concord_close_feature ((COS_Feature)obj);
}


COS_Feature_INDEX
concord_get_feature_index (COS_object ds,
			   COS_object genre, COS_object feature)
{
  if (COS_OBJECT_FEATURE_INDEX_P (feature))
    return feature;
  else
    {
      COS_Feature fobj = concord_get_feature (ds, genre, feature);
      COS_Genre gobj;
      char* feature_name;

      if (fobj == NULL)
	return NULL;

      gobj = concord_feature_get_genre (fobj);
      feature_name = concord_feature_get_name (fobj);
      return concord_genre_get_index (gobj, feature_name);
    }
}

int
cos_retain_index (COS_Object obj)
{
  return concord_close_index ((COS_Feature_INDEX)obj);
}

int
cos_release_index (COS_Object obj)
{
  return concord_close_index ((COS_Feature_INDEX)obj);
}


int
cos_retain_db_object (COS_Object obj)
{
  return 0;
}

int
cos_release_db_object (COS_Object obj)
{
  return 0;
}

COS_object
concord_decode_object (COS_object ds, COS_object genre,
		       COS_object feature, COS_object id)
{
  COS_Feature_INDEX index = concord_get_feature_index (ds, genre, feature);
  char* id_str;
  char buf[256];
  CONCORD_String_Tank obj_st;
  int cid;
  size_t end;

  if (index == NULL)
    return NULL;

  if ( index->decoding_table != NULL )
    {
      COS_object ret = cos_hash_table_get (index->decoding_table, id);
      if (ret != NULL)
	return ret;
    }
  else
    index->decoding_table = cos_make_hash_table ();

  if (COS_OBJECT_INT_P (id))
    {
      snprintf(buf, 256, "%ld", cos_int_value (id));
      id_str = buf;
    }
  else if (COS_OBJECT_CHAR_P (id))
    {
      snprintf(buf, 256, "%d", cos_char_id (id));
      id_str = buf;
    }
  else if (COS_OBJECT_SYMBOL_P (id))
    {
      id_str = cos_string_data (cos_symbol_name ((COS_Symbol)id));
    }
  else if (COS_OBJECT_C_STRING_P (id))
    {
      id_str = (char*)id;
    }
  else if (COS_OBJECT_STRING_P (id))
    {
      id_str = cos_string_data ((COS_String)id);
    }
  else
    return NULL;

  if ( concord_index_strid_get_obj_string (index, id_str, &obj_st) )
    return NULL;

  {
    COS_object obj;

    cid = cos_read_char (CONCORD_String_data (&obj_st),
			 CONCORD_String_size (&obj_st),
			 0, &end);
    if ( cid >= 0 )
      {
	obj = cos_make_char (cid);
	cos_retain_object (id);
	cos_retain_object (obj);
	cos_hash_table_put (index->decoding_table, id, obj);
	return obj;
      }
  }
  return NULL;
}

COS_object
concord_object_get_attribute (COS_object object, COS_object feature)
{
  CONCORD_Genre gobj;
  CONCORD_Feature fobj;
  COS_object ret;

  if (COS_OBJECT_CHAR_P (object))
    {
      if (!COS_OBJECT_DS_P (concord_current_env))
	{
	  concord_current_env = NULL;
	  return NULL;
	}
      gobj = concord_get_genre (concord_current_env, "character");
    }
  else
    return NULL;
  if (gobj == NULL)
    return NULL;

  fobj = concord_get_feature (concord_current_env, gobj, feature);
  if (fobj == NULL)
    return NULL;

  if ( fobj->value_table != NULL )
    {
      ret = cos_hash_table_get (fobj->value_table, object);
      if (ret != NULL)
	return ret;
    }
  else
    fobj->value_table = cos_make_hash_table ();

  {
    char id_buf[256];
    CONCORD_String_Tank val_st;
    size_t end;

    if (COS_OBJECT_CHAR_P (object))
      {
	cos_utf8_print_char (object, (unsigned char*)id_buf, 256);
      }
    else
      return NULL;

    if ( concord_obj_get_feature_value_string (id_buf, fobj, &val_st) )
      return NULL;

    ret = cos_read_object (CONCORD_String_data (&val_st),
			   CONCORD_String_size (&val_st),
			   0, &end);
    if ( ret == NULL )
      return NULL;

    cos_retain_object (object);
    cos_retain_object (ret);
    cos_hash_table_put (fobj->value_table, object, ret);
    return ret;
  }
}
