/* Print functions of CONCORD Object System

   Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _COS_PRINT_H
#define _COS_PRINT_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

#include <stdlib.h>
#include <cos.h>

int
cos_utf8_encode_char (int cid, unsigned char *dest, size_t size);

int
cos_utf8_print_char (COS_object character, unsigned char *dest, size_t size);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_COS_PRINT_H */
