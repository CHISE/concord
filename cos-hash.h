/* Copyright (C) 2013 MORIOKA Tomohiko
   This file is part of the CONCORD Library.

   The CONCORD Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The CONCORD Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the CONCORD Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _COS_HASH_H
#define _COS_HASH_H

#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

#include <concord-name.h>

typedef CONCORD_HASH_TABLE* COS_Hash_Table;
typedef CONCORD_HASH_TABLE_ENTRY COS_Hash_Table_Entry;

COS_Hash_Table cos_make_hash_table (void);

void cos_destroy_hash_table (CONCORD_NAME_TABLE* table);

COS_object cos_hash_table_get (COS_Hash_Table table, COS_object key);

int cos_hash_table_put (COS_Hash_Table table,
			COS_object key, COS_object value);

int cos_hash_table_grow (COS_Hash_Table table);

size_t cos_hash_object (COS_object obj);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* !_COS_HASH_H */
